# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//build/test.gni")
import("../../../../castplus_cast_engine_part.gni")
import("../../../../config.gni")

module_output_path = "multimedia_av_session/session"

###############################################################################
config("module_private_config") {
  visibility = [ ":*" ]

  include_dirs = [
    "../../ipc/base/",
    "../../ipc/proxy/",
    "../../ipc/stub/",
    "../../server/",
    "../../server/migrate/",
    "../../server/softbus/",
    "../../server/remote/",
    "../../../../interfaces/inner_api/native/session/include/",
    "../../../../frameworks/native/session/include",
    "../../adapter/",
    "../../../../utils/include/",
  ]
}

ohos_unittest("MigrateAVSessionTest") {
  module_out_path = module_output_path

  sources = [ "migrate_avsession_test.cpp" ]

  cflags_cc = [ "--coverage" ]

  ldflags = [ "--coverage" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/common:avsession_common",
    "../../../../frameworks/native/session:avsession_client",
    "../../../../utils:avsession_utils",
    "../../../session:avsession_item",
    "../../../session:avsession_server",
    "../../../session:avsession_service",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "audio_framework:audio_client",
    "c_utils:utils",
    "device_manager:devicemanagersdk",
    "dsoftbus:softbus_client",
    "hilog:libhilog",
    "input:libmmi-client",
    "ipc:ipc_single",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("SoftbusSessionManagerTest") {
  module_out_path = module_output_path

  sources = [ "softbus_session_manager_test.cpp" ]

  cflags_cc = [ "--coverage" ]

  ldflags = [ "--coverage" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../session:avsession_server",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "audio_framework:audio_client",
    "c_utils:utils",
    "device_manager:devicemanagersdk",
    "dsoftbus:softbus_client",
    "hilog:libhilog",
    "input:libmmi-client",
    "ipc:ipc_single",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("AVSessionServiceTest") {
  module_out_path = module_output_path

  sources = [ "avsession_service_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../../frameworks/common:avsession_common",
    "../../../../frameworks/native/session:avsession_client",
    "../../../../utils:avsession_utils",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "audio_framework:audio_client",
    "c_utils:utils",
    "device_manager:devicemanagersdk",
    "dsoftbus:softbus_client",
    "hilog:libhilog",
    "image_framework:image_native",
    "input:libmmi-client",
    "ipc:ipc_single",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("AppManagerAdapterTest") {
  module_out_path = module_output_path

  sources = [ "appmanager_adapter_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "./../../../session:avsession_item",
    "./../../../session:avsession_service",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "audio_framework:audio_client",
    "c_utils:utils",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "input:libmmi-client",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("AbilityConnectHelperTest") {
  module_out_path = module_output_path

  sources = [ "ability_connect_helper_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "./../../../session:avsession_item",
    "./../../../session:avsession_service",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "audio_framework:audio_client",
    "c_utils:utils",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "input:libmmi-client",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("BkGrAudioControllerTest") {
  module_out_path = module_output_path

  sources = [ "bkgr_audio_controller_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "./../../../session:avsession_item",
    "./../../../session:avsession_service",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "audio_framework:audio_client",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "input:libmmi-client",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("AudioAdapterTest") {
  module_out_path = module_output_path

  sources = [ "audio_adapter_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../session:avsession_server",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "audio_framework:audio_client",
    "hilog:libhilog",
  ]
}

ohos_unittest("RemoteUtilsTest") {
  module_out_path = module_output_path

  sources = [ "remote_utils_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../session:avsession_server",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:wantagent_innerkits",
    "audio_framework:audio_client",
    "hilog:libhilog",
    "input:libmmi-client",
    "safwk:system_ability_fwk",
  ]
}

ohos_unittest("SessionStackTest") {
  module_out_path = module_output_path

  sources = [ "session_stack_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../session:avsession_server",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "audio_framework:audio_client",
    "c_utils:utils",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "input:libmmi-client",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
}

ohos_unittest("AVRouterTest") {
  module_out_path = module_output_path

  sources = [ "avrouter_test.cpp" ]

  configs = [ ":module_private_config" ]

  deps = [
    "../../../session:avsession_server",
    "//third_party/bounds_checking_function:libsec_shared",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:app_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "access_token:libtokenid_sdk",
    "audio_framework:audio_client",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "data_object:distributeddataobject_impl",
    "device_manager:devicemanagersdk",
    "hilog:libhilog",
    "input:libmmi-client",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  if (castplus_cast_engine_enable) {
    cflags = [ "-DCASTPLUS_CAST_ENGINE_ENABLE" ]
    deps += [ "../../../session:avsession_router" ]
  }
}

if (castplus_cast_engine_enable) {
  ohos_unittest("HwCastTest") {
    module_out_path = module_output_path

    sources = [ "hw_cast_test.cpp" ]

    configs = [ ":module_private_config" ]

    deps = [
      "../../../session:avsession_server",
      "./../../../../frameworks/common:avsession_common",
      "./../../../../frameworks/native/session:avsession_cast_client",
      "./../../../../utils:avsession_utils",
      "./../../../session:avsession_cast_item",
      "./../../../session:avsession_item",
      "./../../../session:avsession_router",
      "//third_party/bounds_checking_function:libsec_shared",
      "//third_party/googletest:gtest_main",
      "//third_party/openssl:libcrypto_shared",
    ]

    external_deps = [
      "ability_base:want",
      "ability_runtime:app_manager",
      "ability_runtime:wantagent_innerkits",
      "access_token:libaccesstoken_sdk",
      "access_token:libtokenid_sdk",
      "audio_framework:audio_client",
      "bundle_framework:appexecfwk_base",
      "bundle_framework:appexecfwk_core",
      "c_utils:utils",
      "cast_engine:cast_engine_client",
      "data_object:distributeddataobject_impl",
      "device_manager:devicemanagersdk",
      "hilog:libhilog",
      "image_framework:image_native",
      "input:libmmi-client",
      "ipc:ipc_single",
      "safwk:system_ability_fwk",
      "samgr:samgr_proxy",
    ]
  }
}

if (castplus_cast_engine_enable) {
  ohos_unittest("HwCastStreamPlayerTest") {
    module_out_path = module_output_path

    sources = [ "hw_cast_stream_player_test.cpp" ]

    configs = [ ":module_private_config" ]

    deps = [
      "../../../../frameworks/common:avsession_common",
      "../../../session:avsession_cast_item",
      "../../../session:avsession_item",
      "../../../session:avsession_router",
      "//third_party/bounds_checking_function:libsec_shared",
      "//third_party/googletest:gtest_main",
    ]

    external_deps = [
      "ability_base:want",
      "ability_runtime:app_manager",
      "ability_runtime:wantagent_innerkits",
      "access_token:libaccesstoken_sdk",
      "access_token:libnativetoken",
      "access_token:libtoken_setproc",
      "audio_framework:audio_client",
      "bundle_framework:appexecfwk_base",
      "bundle_framework:appexecfwk_core",
      "c_utils:utils",
      "cast_engine:cast_engine_client",
      "data_object:distributeddataobject_impl",
      "device_manager:devicemanagersdk",
      "hilog:libhilog",
      "image_framework:image_native",
      "input:libmmi-client",
      "ipc:ipc_single",
      "safwk:system_ability_fwk",
      "samgr:samgr_proxy",
    ]
  }
}

###############################################################################
group("av_session_server_unittest") {
  testonly = true

  deps = [
    ":AVRouterTest",
    ":AVSessionServiceTest",
    ":AbilityConnectHelperTest",
    ":AbilityConnectHelperTest",
    ":AppManagerAdapterTest",
    ":AudioAdapterTest",
    ":BkGrAudioControllerTest",
    ":BkGrAudioControllerTest",
    ":MigrateAVSessionTest",
    ":RemoteUtilsTest",
    ":SessionStackTest",
    ":SoftbusSessionManagerTest",
  ]

  if (castplus_cast_engine_enable) {
    deps += [
      ":HwCastStreamPlayerTest",
      ":HwCastTest",
    ]
  }
}
###############################################################################
